import mysql.connector

mydb = mysql.connector.connect(
  host="127.0.0.1",
  user="root",
  password="password",
  database="my_site"
)

mycursor = mydb.cursor()

def requete(query):
    print("query:", query)
    select = True
    if query[:6] != "select":
        select = False
    mycursor.execute(query)
    myresult = mycursor.fetchall()
    field_names = [i[0] for i in mycursor.description]
    if select == False:
        mydb.commit()
    return (field_names, myresult)
