# import the Flask class from the flask module
from flask import Flask, render_template, redirect, url_for, request
from data.server import *
from tabulate import tabulate
import pandas as pd

# create the application object
app = Flask(__name__)

# user
USER = ""

# my_site pour les inserts
# now
def myformat(table):
    newtable = ""
    if table != "":
        Columns = table[0]
        content = [list(ele) for ele in table[1]] 
        newtable = pd.DataFrame(content, columns=Columns)
    return str(newtable)

# use decorators to link the function to a url
@app.route('/', methods=['GET', 'POST'])
def home():
    tab = ""
    if request.method == 'POST':
        searchfield = request.form['searchfield']
    return render_template('search.html', tab=myformat(tab))  # render a template

@app.route('/welcome')
def welcome():
    return render_template('welcome.html')  # render a template

# Route for handling the login page logic
@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
    return render_template('login.html', error=error)

# Route for handling the register page logic
@app.route('/register', methods=['GET', 'POST'])
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
    return render_template('register.html')

@app.route('/search', methods=['GET', 'POST'])
def search():
    if request.method == 'POST':
        searchfield = request.form['searchfield']
    return render_template('search.html')  # render a template

@app.route('/search_user', methods=['GET', 'POST'])
def search_user():
    if request.method == 'POST':
        searchfield = request.form['searchfield']
    return render_template('search_user.html')  # render a template

@app.route('/create_user', methods=['GET', 'POST'])
def create_user():
    if request.method == 'POST':
        title = request.form['title']
        content = request.form['content']
        return render_template('create_user.html')  # render a template
    return render_template('create_user.html')  # render a template

@app.route('/profile_user', methods=['GET', 'POST'])
def profile_user():
    if request.method == 'POST':
        searchfield = request.form['searchfield']
    return render_template('profile_user.html')  # render a template

# start the server with the 'run()' method
if __name__ == '__main__':
    app.run(debug=True)
